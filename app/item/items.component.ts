import {Component, OnDestroy, OnInit} from "@angular/core";

import { ItemService } from "./item.service";
import {Subscription} from "rxjs/Rx";
import {FileDetail, FileItems} from "~/item/item";
import {confirm, alert, action} from "ui/dialogs";

import { Hprt, HPRTPrinter } from "nativescript-hprt";

@Component({
    selector: "ns-items",
    moduleId: module.id,
    templateUrl: "./items.component.html",
})
export class ItemsComponent implements OnInit, OnDestroy {
    printerStatus: string;
    files: FileItems[];
    items: Array<HPRTPrinter>;
    modelName: string;
    private hprt: Hprt;

    connected: string;
    text: string;
    selected: boolean;
    btEnabled: boolean;

    fileDetail: FileDetail;

    fileSubs: Subscription;
    fileReadSubs: Subscription;
    printReadSubs: Subscription;
    getFilesSubs: Subscription;
    existingFiles: Subscription;
    constructor(private itemService: ItemService) {}

    ngOnInit(): void {
        this.hprt = new Hprt();
        this.connected = "== NOT CONNECTED ==";
        this.printerStatus = 'button button-neutral m2';
        this.selected = false;
        this.btEnabled = this.hprt.isBluetoothEnabled();
        this.loadPrinter();
        this.setUpPrinter();
        this.findExistingFiles();
    }

    private findExistingFiles(){
        this.existingFiles = this.itemService.returnFiles()
            .subscribe(
                res => {
                    this.files = res;
                },
                err => console.log(err)
            );
    }

    public transIdTranslate(val){
        let re = /\s*_\s*/;
        let nameList = val.split(re)[3];
        let id = nameList.slice( 0, 8);
        return id;
    }

    proccessing: boolean = false;

    // TODO: find if files is already connected.
    public printLatest(){

        if(this.proccessing){
            return;
        }

        this.proccessing = true;

        let options = {
            title: "Error Printing",
            message:`Error`,
            okButtonText: "OK"
        };

        const latest = this.files[0];
        let hours = latest.lastModefied.getHours();
        let minutes = latest.lastModefied.getMinutes();
        let ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0'+minutes : minutes;
        let strTime = hours + ':' + minutes + ampm;
        const cst = latest.lastModefied.getMonth()+1 + "/" + latest.lastModefied.getDate() + "/" + latest.lastModefied.getFullYear() + " " + strTime;

        this.printReadSubs = this.itemService.readPrinting(latest.name)
            .subscribe( res => {
                this.proccessing = false;
                this.fileDetail = JSON.parse(res);
                let displayDraw: string;
                switch (this.fileDetail.draw){
                    case 'd1': displayDraw = '11-am';
                        break;
                    case 'd2': displayDraw = '4pm';
                        break;
                    case 'd3': displayDraw = '9pm';
                        break;
                }
                if(!this.connected){
                    alert(options)
                }
                return this.printReceipt(this.fileDetail, cst, displayDraw);
            }, err => {
                alert(options);
                return this.proccessing = false;
            });

    }

    public loadPrinter(){
        this.getFilesSubs = this.fileSubs = this.itemService.getListFiles()
            .subscribe(
                res => {
                    const file: FileItems[] = [];
                    res.forEach((val) => {
                        if(!(val.name && val.name.startsWith('tprint'))){
                            return;
                        }
                        const localFiles = new FileItems(val.name, val.path, val.lastModified);
                        file.push(localFiles)
                    });
                    this.itemService.getPrint(file)
                },
                err => console.log(err)
            );
    }

    // read print receipt
    public readFile(rd: FileItems){

        let hours = rd.lastModefied.getHours();
        let minutes = rd.lastModefied.getMinutes();
        let ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0'+minutes : minutes;
        let strTime = hours + ':' + minutes + ampm;
        const cst = rd.lastModefied.getMonth()+1 + "/" + rd.lastModefied.getDate() + "/" + rd.lastModefied.getFullYear() + " " + strTime;

        this.fileReadSubs = this.itemService.readPrinting(rd.name)
            .subscribe(
                res => {
                    this.fileDetail = JSON.parse(res);
                    let displayDraw: string;
                    switch (this.fileDetail.draw){
                        case 'd1': displayDraw = '11-am';
                            break;
                        case 'd2': displayDraw = '4pm';
                            break;
                        case 'd3': displayDraw = '9pm';
                            break;
                    }

                    let options = {
                        title: `Confirm Print`,
                        message: `${cst} ${displayDraw} Total:${this.fileDetail.total}`,
                        okButtonText: "Yes",
                        cancelButtonText: "No"
                    };

                    confirm(options).then((result: boolean) => {
                        if(!result){
                            return;
                        }
                        return this.printReceipt(this.fileDetail, cst, displayDraw);
                    });
                },
                err => {
                    console.log('ERROR', err)
                }
            )


    }

    // printing
    private setUpPrinter(){
        this.enableBluetooth();
        this.searchPrinters();
    }

    private enableBluetooth() {
        this.hprt.enableBluetooth().then((res) => {
            this.btEnabled = true;
        }, (err) => {
            console.log("Error", err);
        });
    }

    public showPrinter(){
        let options = {
            title: "Printer - Status",
            message: `${this.connected} `,
            okButtonText: "OK"
        };
        alert(options);
    }

    // TODO: REMOVE ALL FILES STARTING WITH NAME -TPRINT-
    public clearFiles(){
        let options = {
            title: "Delete Receipt",
            message: `Are you sure you want to delete all receipts?`,
            okButtonText: "Yes",
            cancelButtonText: "No"
        };
        confirm(options).then((result) => {
            console.log(result)
        });
    }

    private searchPrinters() {
        this.hprt.searchPrinters().then(printers => {
            let hprtPrinter = [];
            if (!printers){
                return;
            }
            printers.forEach(val =>{
                if(val.modelName && val.modelName.startsWith('MTP')){
                    this.modelName = val.modelName;
                    hprtPrinter.push(val);
                }
                return;
            });
            this.items = hprtPrinter;
        }, (err) => {
            console.log("No Printer:", err)
        })
    }

    public selectPrinter(){
        let options = {
            title: "Printer selection",
            message: "Choose your printer",
            cancelButtonText: "Cancel",
            actions: [this.modelName]
        };

        action(options).then((result) => {
            this.items.forEach(val => {
                if(val.modelName === result){
                    this.connect(val);
                } return
            });
        });

    }

    private connect(tprnt) {
        this.hprt.connect(tprnt).then((res) => {
            this.connected = "== PRINTER CONNECTED ==";
            this.printerStatus = 'button button-positive m2';
            this.selected = true;
        }, (err) => {
            this.connected = "== NO CONNECTION ==";
            this.printerStatus = 'button button-neutral m2';
            this.selected = false
        })

    }

    public disconnect() {
        let options = {
            title: "Disconnect Printer",
            message: "Are you sure you want to disconnect?",
            okButtonText: "Yes",
            cancelButtonText: "No"
        };

        confirm(options).then((result: boolean) => {
            if(result){
                this.hprt.disconnect().then((res) => {
                    this.connected = "== NO CONNECTION ==";
                    this.printerStatus = 'button button-neutral m2';
                    this.selected = false;
                }, (err) => {
                    console.log("error", err)
                })
            } return
        });


    }

    private printReceipt(trns: FileDetail, time: any, displayDraw: any) {
        // https://github.com/krushkamx/nativescript-hprt

        // Sum this numbers to get attribute combination
        // Example:
        // Double height: 16
        // Double Width: 32
        // Underline: 4
        // Bold: 2
        // Mini: 1
        // White text: 8
        this.hprt.printText("Piso Gaming", 1, 2, 0);
        this.hprt.printTextMini(`transId: ${trns.transactionId}`);
        this.hprt.printTextMini(`${time} - ${displayDraw}`);
        this.hprt.horizontalLine();
        this.hprt.printTextMini(`#  - combination  -  amount  -  prize`);
        trns.bets.forEach((val, index) => {
            let displayRaffle: string = '';
            switch (val.raffle){
                case 's3': displayRaffle = '';
                    break;
                case 's3r': displayRaffle = 'R';
                    break;
            }
            let paddedComb = this.itemService.leftPaddy(val.combination, 3);

            // replace the element with space if the length of string is not 4
            const combRafle = paddedComb + displayRaffle;
            const cominationRaffle  = this.itemService.rightPaddy(combRafle, 9, ' ');

            // replace the element with space if the length of string is not 8
            const amount = this.itemService.rightPaddy(val.amount, 6 , ' ');
            const i = index + 1;
            const indexBet = this.itemService.rightPaddy(i, 3, ' ');

            this.hprt.printTextMini(`${indexBet}-     ${cominationRaffle}-    ${amount}-  ${val.amount * val.unitPrize}`);
        });
        this.hprt.horizontalLine();
        this.hprt.printTextMini(`Total                  ${trns.total}`);
        this.hprt.printTextMini(`Outlet: ${trns._.outlet && trns._.outlet ? trns._.outlet: 'Do not receive: No Outlet Name'}`);
        this.hprt.printText("Thank you, Please come Again", 1, 16, 0);
        this.hprt.newLine(2);
    }


    ngOnDestroy(){
        this.fileSubs.unsubscribe();
        this.fileReadSubs.unsubscribe();
        this.printReadSubs.unsubscribe();
        this.getFilesSubs.unsubscribe();
        this.existingFiles.unsubscribe();
    }
}
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var FileItems = /** @class */ (function () {
    function FileItems(name, path, lastModefied) {
        this.name = name;
        this.path = path;
        this.lastModefied = lastModefied;
    }
    return FileItems;
}());
exports.FileItems = FileItems;
var Bet = /** @class */ (function () {
    function Bet(raffle, combination, amount, unitPrize, id) {
        this.raffle = raffle;
        this.combination = combination;
        this.amount = amount;
        this.unitPrize = unitPrize;
        this.id = id;
    }
    return Bet;
}());
exports.Bet = Bet;
var User = /** @class */ (function () {
    function User(outlet, branch) {
        this.outlet = outlet;
        this.branch = branch;
    }
    return User;
}());
exports.User = User;
var FileDetail = /** @class */ (function () {
    function FileDetail(total, transactionId, draw, date, bets, _) {
        this.total = total;
        this.transactionId = transactionId;
        this.draw = draw;
        this.date = date;
        this.bets = bets;
        this._ = _;
    }
    return FileDetail;
}());
exports.FileDetail = FileDetail;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaXRlbS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIml0ZW0udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFNQTtJQUNJLG1CQUNXLElBQVcsRUFDWCxJQUFXLEVBQ1gsWUFBZ0I7UUFGaEIsU0FBSSxHQUFKLElBQUksQ0FBTztRQUNYLFNBQUksR0FBSixJQUFJLENBQU87UUFDWCxpQkFBWSxHQUFaLFlBQVksQ0FBSTtJQUN6QixDQUFDO0lBQ1AsZ0JBQUM7QUFBRCxDQUFDLEFBTkQsSUFNQztBQU5ZLDhCQUFTO0FBT3RCO0lBQ0ksYUFDVyxNQUFhLEVBQ2IsV0FBa0IsRUFDbEIsTUFBYyxFQUNkLFNBQWdCLEVBQ2hCLEVBQVM7UUFKVCxXQUFNLEdBQU4sTUFBTSxDQUFPO1FBQ2IsZ0JBQVcsR0FBWCxXQUFXLENBQU87UUFDbEIsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUNkLGNBQVMsR0FBVCxTQUFTLENBQU87UUFDaEIsT0FBRSxHQUFGLEVBQUUsQ0FBTztJQUNsQixDQUFDO0lBQ1AsVUFBQztBQUFELENBQUMsQUFSRCxJQVFDO0FBUlksa0JBQUc7QUFXaEI7SUFDSSxjQUNXLE1BQWEsRUFDYixNQUFhO1FBRGIsV0FBTSxHQUFOLE1BQU0sQ0FBTztRQUNiLFdBQU0sR0FBTixNQUFNLENBQU87SUFDdEIsQ0FBQztJQUNQLFdBQUM7QUFBRCxDQUFDLEFBTEQsSUFLQztBQUxZLG9CQUFJO0FBTWpCO0lBQ0ksb0JBQ1csS0FBWSxFQUNaLGFBQW9CLEVBQ3BCLElBQVksRUFDWixJQUFTLEVBQ1QsSUFBVyxFQUNYLENBQU87UUFMUCxVQUFLLEdBQUwsS0FBSyxDQUFPO1FBQ1osa0JBQWEsR0FBYixhQUFhLENBQU87UUFDcEIsU0FBSSxHQUFKLElBQUksQ0FBUTtRQUNaLFNBQUksR0FBSixJQUFJLENBQUs7UUFDVCxTQUFJLEdBQUosSUFBSSxDQUFPO1FBQ1gsTUFBQyxHQUFELENBQUMsQ0FBTTtJQUNoQixDQUFDO0lBQ1AsaUJBQUM7QUFBRCxDQUFDLEFBVEQsSUFTQztBQVRZLGdDQUFVIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGludGVyZmFjZSBJdGVtIHtcbiAgICBpZDogbnVtYmVyO1xuICAgIG5hbWU6IHN0cmluZztcbiAgICByb2xlOiBzdHJpbmc7XG59XG5cbmV4cG9ydCBjbGFzcyBGaWxlSXRlbXN7XG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIHB1YmxpYyBuYW1lOnN0cmluZyxcbiAgICAgICAgcHVibGljIHBhdGg6c3RyaW5nLFxuICAgICAgICBwdWJsaWMgbGFzdE1vZGVmaWVkOmFueSxcbiAgICApe31cbn1cbmV4cG9ydCBjbGFzcyBCZXR7XG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIHB1YmxpYyByYWZmbGU6c3RyaW5nLFxuICAgICAgICBwdWJsaWMgY29tYmluYXRpb246bnVtYmVyLFxuICAgICAgICBwdWJsaWMgYW1vdW50OiBudW1iZXIsXG4gICAgICAgIHB1YmxpYyB1bml0UHJpemU6bnVtYmVyLFxuICAgICAgICBwdWJsaWMgaWQ6bnVtYmVyLFxuICAgICl7fVxufVxuXG5cbmV4cG9ydCBjbGFzcyBVc2Vye1xuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBwdWJsaWMgb3V0bGV0OnN0cmluZyxcbiAgICAgICAgcHVibGljIGJyYW5jaDpzdHJpbmcsXG4gICAgKXt9XG59XG5leHBvcnQgY2xhc3MgRmlsZURldGFpbHtcbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgcHVibGljIHRvdGFsOm51bWJlcixcbiAgICAgICAgcHVibGljIHRyYW5zYWN0aW9uSWQ6c3RyaW5nLFxuICAgICAgICBwdWJsaWMgZHJhdzogc3RyaW5nLFxuICAgICAgICBwdWJsaWMgZGF0ZTogYW55LFxuICAgICAgICBwdWJsaWMgYmV0czogQmV0W10sXG4gICAgICAgIHB1YmxpYyBfOiBVc2VyXG4gICAgKXt9XG59Il19
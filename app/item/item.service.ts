import { Injectable } from "@angular/core";

import { Item } from "./item";
import * as fs from "tns-core-modules/file-system";
import {FileItems} from "~/item/item";
import {Observable, Subject} from "rxjs/Rx";
declare var android: any;
@Injectable()
export class ItemService {

    private files = new Subject<FileItems[]>();

    returnFiles(){
        return this.files.asObservable();
    }
    getPrint(print: FileItems[]){
        const localprint = print.sort((a,b) => {
            return b.lastModefied - a.lastModefied;
        });
        return this.files.next(localprint);
    }

    findFiles(){
        let DocPath = fs.path.join(android.os.Environment.getExternalStoragePublicDirectory(android.os.Environment.DIRECTORY_DOWNLOADS).toString());
        let sdPath = fs.Folder.fromPath(DocPath);
        const p1 = sdPath.getEntities();
        return Observable.fromPromise(p1);
    }

    getListFiles(){
        return this.findFiles();
    }

    // read file of seleted printing
    readPrinting(filename: string){
        // read content file
        let path = fs.path.join(android.os.Environment.getExternalStoragePublicDirectory(android.os.Environment.DIRECTORY_DOWNLOADS).toString(), filename);
        let file = fs.File.fromPath(path);
        const p2 = file.readText();
        return Observable.fromPromise(p2)
    }

    // https://stackoverflow.com/questions/1267283/how-can-i-pad-a-value-with-leading-zeros
    // adds zero to number according to padlen
    // var fu = paddy(14, 5); // 00014
    // var bar = paddy(2, 4, '#'); // ###2
    leftPaddy(num, padlen, padchar?) {
        let pad_char = typeof padchar !== 'undefined' ? padchar : '0';
        let pad = new Array(1 + padlen).join(pad_char);
        return (pad + num).slice(-pad.length);
    }

    rightPaddy(num, padlen, padchar) {
        var pad_char = typeof padchar !== 'undefined' ? padchar : '0';
        num = num + '';
        padlen = padlen - num.length;
        var pad = new Array(1 + padlen).join(pad_char);
        return (num + pad);
    }
}

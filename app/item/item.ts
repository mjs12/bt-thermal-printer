export interface Item {
    id: number;
    name: string;
    role: string;
}

export class FileItems{
    constructor(
        public name:string,
        public path:string,
        public lastModefied:any,
    ){}
}
export class Bet{
    constructor(
        public raffle:string,
        public combination:number,
        public amount: number,
        public unitPrize:number,
        public id:number,
    ){}
}


export class User{
    constructor(
        public outlet:string,
        public branch:string,
    ){}
}
export class FileDetail{
    constructor(
        public total:number,
        public transactionId:string,
        public draw: string,
        public date: any,
        public bets: Bet[],
        public _: User
    ){}
}